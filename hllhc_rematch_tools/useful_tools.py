# %% Round some values
import os


def round_phases(collider):
    for beam in ["b1", "b2"]:
        collider.varval[f"muxip1{beam}"] = round(collider.varval[f"muxip1{beam}"], 4)
        collider.varval[f"muyip1{beam}"] = round(collider.varval[f"muyip1{beam}"], 4)
        collider.varval[f"muxip2{beam}"] = round(collider.varval[f"muxip2{beam}"], 4)
        collider.varval[f"muyip2{beam}"] = round(collider.varval[f"muyip2{beam}"], 4)
        collider.varval[f"muxip3{beam}"] = round(collider.varval[f"muxip3{beam}"], 4)
        collider.varval[f"muyip3{beam}"] = round(collider.varval[f"muyip3{beam}"], 4)
        collider.varval[f"muxip4{beam}"] = round(collider.varval[f"muxip4{beam}"], 4)
        collider.varval[f"muyip4{beam}"] = round(collider.varval[f"muyip4{beam}"], 4)
        collider.varval[f"muxip5{beam}"] = round(collider.varval[f"muxip5{beam}"], 4)
        collider.varval[f"muyip5{beam}"] = round(collider.varval[f"muyip5{beam}"], 4)
        collider.varval[f"muxip6{beam}"] = round(collider.varval[f"muxip6{beam}"], 4)
        collider.varval[f"muyip6{beam}"] = round(collider.varval[f"muyip6{beam}"], 4)
        collider.varval[f"muxip7{beam}"] = round(collider.varval[f"muxip7{beam}"], 4)
        collider.varval[f"muyip7{beam}"] = round(collider.varval[f"muyip7{beam}"], 4)
        collider.varval[f"muxip8{beam}"] = round(collider.varval[f"muxip8{beam}"], 4)
        collider.varval[f"muyip8{beam}"] = round(collider.varval[f"muyip8{beam}"], 4)
        collider.varval[f"betxip1{beam}"] = round(collider.varval[f"betxip1{beam}"], 3)
        collider.varval[f"betyip1{beam}"] = round(collider.varval[f"betyip1{beam}"], 3)
        collider.varval[f"betxip2{beam}"] = round(collider.varval[f"betxip2{beam}"], 3)
        collider.varval[f"betyip2{beam}"] = round(collider.varval[f"betyip2{beam}"], 3)
        collider.varval[f"betxip5{beam}"] = round(collider.varval[f"betxip5{beam}"], 3)
        collider.varval[f"betyip5{beam}"] = round(collider.varval[f"betyip5{beam}"], 3)
        collider.varval[f"betxip8{beam}"] = round(collider.varval[f"betxip8{beam}"], 3)
        collider.varval[f"betyip8{beam}"] = round(collider.varval[f"betyip8{beam}"], 3)
        collider.varval[f"alfxip1{beam}"] = 0
        collider.varval[f"alfyip1{beam}"] = 0
        collider.varval[f"alfxip2{beam}"] = 0
        collider.varval[f"alfyip2{beam}"] = 0
        collider.varval[f"alfxip5{beam}"] = 0
        collider.varval[f"alfyip5{beam}"] = 0
        collider.varval[f"alfxip8{beam}"] = 0
        collider.varval[f"alfyip8{beam}"] = 0
        collider.varval[f"dxip1{beam}"] = round(collider.varval[f"dxip1{beam}"], 3)
        collider.varval[f"dpxip1{beam}"] = round(collider.varval[f"dpxip1{beam}"], 3)
        collider.varval[f"dxip2{beam}"] = round(collider.varval[f"dxip2{beam}"], 3)
        collider.varval[f"dpxip2{beam}"] = round(collider.varval[f"dpxip2{beam}"], 3)
        collider.varval[f"dxip5{beam}"] = round(collider.varval[f"dxip5{beam}"], 3)
        collider.varval[f"dpxip5{beam}"] = round(collider.varval[f"dpxip5{beam}"], 3)
        collider.varval[f"dxip8{beam}"] = round(collider.varval[f"dxip8{beam}"], 3)
        collider.varval[f"dpxip8{beam}"] = round(collider.varval[f"dpxip8{beam}"], 3)


def create_symlink(target, link_name):
    try:
        # Remove the existing symbolic link if it exists
        if os.path.islink(link_name):
            os.unlink(link_name)
        os.symlink(target, link_name)
        return f"Symbolic link '{link_name}' created successfully."
    except Exception as e:
        return f"An error occurred: {e}"


def set_sext_all(collider, ks):
    for bn in ["b1", "b2"]:
        for flag in ["d", "f"]:
            myks = ks[bn][flag]
            for sext_flag in ["1", "2"]:
                for arc in ["12", "23", "34", "45", "56", "67", "78", "81"]:
                    # print(f'ks{flag}{sext_flag}.a{arc}{bn}', myks)
                    collider.vars[f"ks{flag}{sext_flag}.a{arc}{bn}"] = myks


def redefine_mcbx_correctors_ip2_ip8(collider, ip):
    mcbx_names = (
        "acbxhv1.lirn acbxhv1.rirn acbxhv2.lirn acbxhv2.rirn acbxhv3.lirn acbxhv3.rirn"
    )

    for n in mcbx_names.split():
        for hv in ["h", "v"]:
            mn = n.replace("hv", hv).replace("irn", str(ip))
            collider.vars[mn] = (
                collider.vars[f"{mn}_from_on_x{ip}{hv}"]._expr
                + collider.vars[f"{mn}_from_on_sep{ip}{hv}"]._expr
            )


def redefine_mcby_correctors_ip2_ip8(collider, ip):
    mcby_names = "acbyhvs4.lirnbim acbyhvs4.rirnbim"

    for bim in ["b1", "b2"]:
        for n in mcby_names.split():
            for hv in ["h", "v"]:
                mn = n.replace("bim", bim).replace("hv", hv).replace("irn", str(ip))
                collider.vars[mn] = (
                    collider.vars[f"{mn}_from_on_x{ip}{hv}"]._expr
                    + collider.vars[f"{mn}_from_on_sep{ip}{hv}"]._expr
                    + collider.vars[f"{mn}_from_on_o{ip}{hv}"]._expr
                    + collider.vars[f"{mn}_from_on_a{ip}{hv}"]._expr
                )

    names_ip2 = (
        "acbyhs5.l2b1 acbyvs5.l2b1 acbcvs5.r2b1 acbchs5.r2b1 "
        "acbyhs5.l2b2 acbyvs5.l2b2 acbcvs5.r2b2 acbchs5.r2b2"
    )

    names_ip8 = (
        "acbchs5.l8b1 acbcvs5.l8b1 acbyvs5.r8b1 acbyhs5.r8b1 "
        "acbchs5.l8b2 acbcvs5.l8b2 acbyvs5.r8b2 acbyhs5.r8b2"
    )

    name_rest = names_ip2 if ip == 2 else names_ip8

    for n in name_rest.split():
        hv = n[4]
        collider.vars[n] = (
            collider.vars[f"{n}_from_on_x{ip}{hv}"]._expr
            + collider.vars[f"{n}_from_on_sep{ip}{hv}"]._expr
            + collider.vars[f"{n}_from_on_o{ip}{hv}"]._expr
            + collider.vars[f"{n}_from_on_a{ip}{hv}"]._expr
        )


def redefine_orbit_correctors_ip2_ip8(collider):
    for ip in [2, 8]:
        redefine_mcbx_correctors_ip2_ip8(collider, ip)
        redefine_mcby_correctors_ip2_ip8(collider, ip)


import matplotlib.pyplot as plt


def plot_orbit(collider, line_name, tw=None):
    if tw is None:
        tw = (
            collider[line_name]
            .cycle("ip3")
            .twiss(method="4d", reverse=True if line_name == "lhcb2" else False)
        )

    fig, axs = plt.subplots(figsize=(21, 11))
    axs.set_title(f"{line_name}")
    axs.plot(tw.s, tw.x, label="x", color="black")
    axs.plot(tw.s, tw.y, label="y", color="red")
    axs_t = axs.twiny()
    axs_t.set_xticks(tw[["s"], "ip.*"], tw[["name"], "ip.*"], rotation="horizontal")
    axs_t.set_xlim(axs.get_xlim()[0], axs.get_xlim()[1])
    axs.set_ylabel(r"$co [m]$")
    axs.set_xlabel(r"$s [m]$")
    axs.legend()
    axs.grid()
    plt.show()


def plot_w(collider, line_name, tw=None):
    if tw is None:
        tw = (
            collider[line_name]
            .cycle("ip3")
            .twiss(method="4d", reverse=True if line_name == "lhcb2" else False)
        )

    fig, axs = plt.subplots(figsize=(21, 11))
    axs.plot(tw.s, tw.wx_chrom, label=rf"$W_x$, {line_name}", color="black")
    axs.plot(tw.s, tw.wy_chrom, label=rf"$W_y$, {line_name}", color="red")
    axs_t = axs.twiny()
    axs_t.set_xticks(tw[["s"], "ip.*"], tw[["name"], "ip.*"])
    axs_t.set_xlim(axs.get_xlim())
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw[["s"], "ip.*"]]
    axs.set_xlabel("s [m]")
    axs.set_ylabel(r"$W_{x,y}$")
    axs.legend()
    plt.show()


def plot_var(collider, line_name, tw=None, var_names=["betx", "bety"], ips=False):
    label_names_dict = {
        "x": "x",
        "y": "y",
        "betx": "\\beta_x",
        "bety": "\\beta_y",
        "wx_chrom": "W_x",
        "wy_chrom": "W_y",
        "dmux": "\\Delta\\mu_x",
        "dmuy": "\\Delta\\mu_y",
    }

    labelx = label_names_dict[var_names[0]]
    labely = label_names_dict[var_names[1]]
    if tw is None:
        tw = collider[line_name].twiss()
    fig, axs = plt.subplots(figsize=(21, 11))
    axs.plot(tw.s, tw[var_names[0]], label=rf"${labelx}$, {line_name}", color="black")
    axs.plot(tw.s, tw[var_names[1]], label=rf"${labely}$, {line_name}", color="red")
    axs_t = axs.twiny()
    axs_t.set_xticks(tw[["s"], "ip.*"], tw[["name"], "ip.*"])
    axs_t.set_xlim(axs.get_xlim())
    if ips:
        [axs_t.axvline(xpos, linestyle="--") for xpos in tw[["s"], "ip.*"]]
    axs.set_xlabel("s [m]")
    axs.set_ylabel(rf"${labelx}, {labely}$")
    axs.legend()
    plt.show()


def set_vars(collider, config):
    print(config)
    for key, val in config.items():
        collider.varval[key] = val


configs = {
    "on_0": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 0,
        "on_sep1": 0,
        "on_sep5": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
        "on_a1": 0,
        "on_a5": 0,
        "on_o1": 0,
        "on_o5": 0,
    },
    "on_x1hs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x1hl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x1vs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x1vl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5hs": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5hl": {
        "cd2q4": 1,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5vs": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x5vl": {
        "cd2q4": 1,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x15hvs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x15hvl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_sep1v": {"on_sep1": 2, "phi_ir1": 0},
    "on_sep5h": {"on_sep5": 2, "phi_ir5": 90},
    "on_sep1h": {"on_sep1": 2, "phi_ir1": 90},
    "on_sep5v": {"on_sep5": 2, "phi_ir5": 0},
    "on_a1v": {"on_a1": 1},
    "on_a1h": {"on_a1": 1, "phi_ir1": 90},
    "on_a5h": {"on_a5": 1},
    "on_a5v": {"on_a5": 1, "phi_ir5": 0},
    "on_o1h": {"on_o1": 1, "phi_ir1": 0},
    "on_o1v": {"on_o1": 1, "phi_ir1": 90},
    "on_o5h": {"on_o5": 1, "phi_ir5": 0},
    "on_o5v": {"on_o5": 1, "phi_ir5": 90},
}
