
#%%
import os
import numpy as np
import pandas as pd
import xtrack as xt
import xpart as xp
import xtrack._temp.lhc_match as lm
import sys
sys.path.append("../")
import hllhc_rematch_tools as lm_updated
import user_defined_functions as udf


# I want to force reload the module udf
from importlib import reload
reload(udf)
reload(lm_updated)


#%% Build collider as defined in user_defined_functions
collider_name = "collider_000_from_madx_thin_15cm_16.json"

if not os.path.exists(f"colliders/{collider_name}"):
    collider = udf.build_my_collider_16(save_to=collider_name)
    collider.to_json(collider_name)


#%% Load collider
collider = xt.Multiline.from_json(f"colliders/{collider_name}")
collider_0 = collider.copy()
#%% User-defined sanity check
udf.check_after_loading_collider(collider)

# %% Set beta* presqueze and squeeze

staged_match = True
default_tol = {None: 1e-8, 'betx': 1e-6, 'bety': 1e-6}

optimizers = {}
betx0_ip1 = 0.5
bety0_ip1 = 0.5
betx0_ip5 = 0.5
bety0_ip5 = 0.5

betx_ip1 = 0.15
bety_ip1 = 0.15
betx_ip5 = 0.15
bety_ip5 = 0.15

# %% Set limits in the strengths
lm_updated.set_var_limits_and_steps(collider)
lm_updated.set_var_limits_and_steps_orbit_ip15(collider)
lm_updated.set_var_limits_and_steps_sextupoles_w(collider)
lm_updated.set_var_limits_and_steps_ksf_ksd(collider)
lm_updated.set_var_limits_and_steps_disp_correctors(collider)
lm_updated.set_var_limits_and_steps_ahvcrab(collider)
udf.check_after_strength_limits(collider)

# %% Rematch all arcs
optimizers["arcs"] = {}
lm_updated.round_phases(collider)
arc_periodic_solution = lm.get_arc_periodic_solution(collider)

rematch_arcs = True

udf.check_before_arcs_rematch(collider)

if rematch_arcs:
    
    for aa in lm.ARC_NAMES:
        print(f"Matching arc {aa}")

        target_mux_b1 = collider.varval[f'mux{aa}b1']
        target_muy_b1 = collider.varval[f'muy{aa}b1']
        target_mux_b2 = collider.varval[f'mux{aa}b2']
        target_muy_b2 = collider.varval[f'muy{aa}b2']

        opt = lm.match_arc_phase_advance(collider=collider, arc_name=aa,
                        target_mux_b1=target_mux_b1, target_muy_b1=target_muy_b1,
                        target_mux_b2=target_mux_b2, target_muy_b2=target_muy_b2)
        optimizers["arcs"][aa] = opt


    arc_periodic_solution = lm.get_arc_periodic_solution(collider)

    udf.check_after_arcs_rematch(collider)

#%% Rematch IR15 with beta* presqueeze

udf.check_before_ir15_rematch(collider)

#collider.vars.vary_default.update(
#    {
#        "kqx1.l1": {"step": 1e-6, "limits": (-qtlim1, 0)},
#        "kqx2a.l1": {"step": 1e-6, "limits": (-qtlim1, 0)},
#        "kqx3.l1": {"step": 1e-6, "limits": (-qtlim1, 0)},
#    }
#)

optimizers["ir15"] = lm_updated.rematch_ir15(
    collider,
    betx0_ip5,
    bety0_ip5,
    tw_sq_a45_ip5_a56=None, 
    restore=False, # restore is set to false to allow for several consecutive runs of the matching
    ir5q4sym=0,
    ir5q5sym=0,
    ir5q6sym=0,
    solve=True,
    match_on_triplet=0,
    imb = 1.5,
    default_tol=default_tol,
)   

udf.check_after_ir15_rematch(collider)

#%%

udf.check_before_ir3_rematch(collider)
tw = collider.twiss()


for bn in ["b1", "b2"]:
    alfx_ip3 = collider.varval[f'alfxip3{bn}']
    alfy_ip3 = collider.varval[f'alfyip3{bn}']
    betx_ip3 = collider.varval[f'betxip3{bn}']
    bety_ip3 = collider.varval[f'betyip3{bn}']
    dx_ip3 = collider.varval[f'dxip3{bn}']
    dpx_ip3 = collider.varval[f'dpxip3{bn}']
    mux_ir3 = collider.varval[f'muxip3{bn}']
    muy_ir3 = collider.varval[f'muyip3{bn}']
    optimizers[f"ir3{bn}"] = lm_updated.rematch_ir3(
        collider=collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=arc_periodic_solution[f"lhc{bn}"]["23"],
        boundary_conditions_right=arc_periodic_solution[f"lhc{bn}"]["34"],
        mux_ir3=mux_ir3,
        muy_ir3=muy_ir3,
        alfx_ip3=alfx_ip3,
        alfy_ip3=alfy_ip3,
        betx_ip3=betx_ip3,
        bety_ip3=bety_ip3,
        dx_ip3=dx_ip3,
        dpx_ip3=dpx_ip3,
        solve=False,
        staged_match=True,
        default_tol=default_tol,
    )

udf.check_after_ir3_rematch(collider)


udf.check_before_ir7_rematch(collider)


for bn in ["b1", "b2"]:
    alfx_ip7 = collider.varval[f'alfxip7{bn}']
    alfy_ip7 = collider.varval[f'alfyip7{bn}']
    betx_ip7 = collider.varval[f'betxip7{bn}']
    bety_ip7 = collider.varval[f'betyip7{bn}']
    dx_ip7 = collider.varval[f'dxip7{bn}']
    dpx_ip7 = collider.varval[f'dpxip7{bn}']
    mux_ir7 = collider.varval[f'muxip7{bn}']
    muy_ir7 = collider.varval[f'muyip7{bn}']
    optimizers[f"ir7{bn}"] = lm_updated.rematch_ir7(
        collider=collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=arc_periodic_solution[f"lhc{bn}"]["67"],
        boundary_conditions_right=arc_periodic_solution[f"lhc{bn}"]["78"],
        mux_ir7=mux_ir7,
        muy_ir7=muy_ir7,
        alfx_ip7=alfx_ip7,
        alfy_ip7=alfy_ip7,
        betx_ip7=betx_ip7,
        bety_ip7=bety_ip7,
        dx_ip7=dx_ip7,
        dpx_ip7=dpx_ip7,
        solve=True,
        staged_match=True,
        default_tol=default_tol,
    )

udf.check_after_ir7_rematch(collider)
# %% Compute target phase advances in auxiliary to ATS IRs for beta* squeeze
tw_sq_a81_ip1_a12 ={}
tw_sq_a45_ip5_a56 = {}

for bn in ['b1', 'b2']:
    tw_sq_a81_ip1_a12[bn] = lm.propagate_optics_from_beta_star(collider, ip_name='ip1',
                line_name=f'lhc{bn}', start=f's.ds.r8.{bn}', end=f'e.ds.l2.{bn}',
                beta_star_x=betx_ip1, beta_star_y=bety_ip1)

    tw_sq_a45_ip5_a56[bn] = lm.propagate_optics_from_beta_star(collider, ip_name='ip5',
                line_name=f'lhc{bn}', start=f's.ds.r4.{bn}', end=f'e.ds.l6.{bn}',
                beta_star_x=betx_ip5, beta_star_y=bety_ip5)

bn = ['b1', 'b2']
mux_ir2_target = {bn: None for bn in bn}
muy_ir2_target = {bn: None for bn in bn}
mux_ir4_target = {bn: None for bn in bn}
muy_ir4_target = {bn: None for bn in bn}
mux_ir6_target = {bn: None for bn in bn}
muy_ir6_target = {bn: None for bn in bn}
mux_ir8_target = {bn: None for bn in bn}
muy_ir8_target = {bn: None for bn in bn}

for bn in ['b1', 'b2']:
    muxip1_l = collider.varval[f'muxip1{bn}_l']
    muyip1_l = collider.varval[f'muyip1{bn}_l']
    muxip1_r = collider.varval[f'muxip1{bn}_r']
    muyip1_r = collider.varval[f'muyip1{bn}_r']

    muxip5_l = collider.varval[f'muxip5{bn}_l']
    muyip5_l = collider.varval[f'muyip5{bn}_l']
    muxip5_r = collider.varval[f'muxip5{bn}_r']
    muyip5_r = collider.varval[f'muyip5{bn}_r']

    muxip2 = collider.varval[f'muxip2{bn}']
    muyip2 = collider.varval[f'muyip2{bn}']
    muxip4 = collider.varval[f'muxip4{bn}']
    muyip4 = collider.varval[f'muyip4{bn}']
    muxip6 = collider.varval[f'muxip6{bn}']
    muyip6 = collider.varval[f'muyip6{bn}']
    muxip8 = collider.varval[f'muxip8{bn}']
    muyip8 = collider.varval[f'muyip8{bn}']

    mux12 = collider.varval[f'mux12{bn}']
    muy12 = collider.varval[f'muy12{bn}']
    mux45 = collider.varval[f'mux45{bn}']
    muy45 = collider.varval[f'muy45{bn}']
    mux56 = collider.varval[f'mux56{bn}']
    muy56 = collider.varval[f'muy56{bn}']
    mux81 = collider.varval[f'mux81{bn}']
    muy81 = collider.varval[f'muy81{bn}']

    mux_ir2_target[bn], muy_ir2_target[bn], mux_ir4_target[bn], muy_ir4_target[bn], mux_ir6_target[bn], muy_ir6_target[bn], mux_ir8_target[bn], muy_ir8_target[bn] = lm.compute_ats_phase_advances_for_auxiliary_irs(f'lhc{bn}', 
                                                tw_sq_a81_ip1_a12[bn], tw_sq_a45_ip5_a56[bn],
                                                muxip1_l, muyip1_l, muxip1_r, muyip1_r,
                                                muxip5_l, muyip5_l, muxip5_r, muyip5_r,
                                                muxip2, muyip2, muxip4, muyip4, muxip6, muyip6, muxip8, muyip8,
                                                mux12, muy12, mux45, muy45, mux56, muy56, mux81, muy81)

udf.check_ats_targets_and_twiss(collider, 
                                tw_sq_a81_ip1_a12= tw_sq_a81_ip1_a12, 
                                tw_sq_a45_ip5_a56=tw_sq_a45_ip5_a56, 
                                arc_periodic_solution=arc_periodic_solution,
                                mux_ir2_target=mux_ir2_target, 
                                muy_ir2_target=muy_ir2_target, 
                                mux_ir4_target=mux_ir4_target, muy_ir4_target=muy_ir4_target)

#%% Rematch IR2

udf.check_before_ir2_rematch(collider)

for bn in ['b1', 'b2']:
    alfx_ip2 = collider.varval[f'alfxip2{bn}']
    alfy_ip2 = collider.varval[f'alfyip2{bn}']
    betx_ip2 = collider.varval[f'betxip2{bn}']
    bety_ip2 = collider.varval[f'betyip2{bn}']
    dx_ip2 = collider.varval[f'dxip2{bn}']
    dpx_ip2 = collider.varval[f'dpxip2{bn}']
    mux_ir2 = collider.varval[f'muxip2{bn}']
    muy_ir2 = collider.varval[f'muyip2{bn}']

    optimizers[f"ir2{bn}"] = lm.rematch_ir2(
        collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=tw_sq_a81_ip1_a12[bn],
        boundary_conditions_right=arc_periodic_solution[f"lhc{bn}"]["23"],
        mux_ir2=mux_ir2_target[bn],
        muy_ir2=muy_ir2_target[bn],
        betx_ip2=betx_ip2,
        bety_ip2=bety_ip2,
        solve=True,
        staged_match=staged_match,
        default_tol=default_tol,
    )
    
udf.check_after_ir2_rematch(collider)

#%% Rematch IR4

udf.check_before_ir4_rematch(collider)

for bn in ['b1', 'b2']:
    alfx_ip4 = collider.varval[f'alfxip4{bn}']
    alfy_ip4 = collider.varval[f'alfyip4{bn}']
    betx_ip4 = collider.varval[f'betxip4{bn}']
    bety_ip4 = collider.varval[f'betyip4{bn}']
    dx_ip4 = collider.varval[f'dxip4{bn}']
    dpx_ip4 = collider.varval[f'dpxip4{bn}']
    mux_ir4 = collider.varval[f'muxip4{bn}']
    muy_ir4 = collider.varval[f'muyip4{bn}']

    optimizers[f"ir4{bn}"] = lm.rematch_ir4(
        collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=arc_periodic_solution[f"lhc{bn}"]["34"],
        boundary_conditions_right=tw_sq_a45_ip5_a56[bn],
        mux_ir4=mux_ir4_target[bn],
        muy_ir4=muy_ir4_target[bn],
        betx_ip4=betx_ip4,
        bety_ip4=bety_ip4,
        alfx_ip4=alfx_ip4,
        alfy_ip4=alfy_ip4,
        dx_ip4=dx_ip4,
        dpx_ip4=dpx_ip4,
        solve=True,
        staged_match=staged_match,
        default_tol=default_tol,
    )

udf.check_after_ir4_rematch(collider)

##%% Rematch IR6

udf.check_before_ir6_rematch(collider)

for bn in ['b1', 'b2']:
    alfx_ip6 = collider.varval[f'alfxip6{bn}']
    alfy_ip6 = collider.varval[f'alfyip6{bn}']
    betx_ip6 = collider.varval[f'betxip6{bn}']
    bety_ip6 = collider.varval[f'betyip6{bn}']
    dx_ip6 = collider.varval[f'dxip6{bn}']
    dpx_ip6 = collider.varval[f'dpxip6{bn}']
    mux_ir6 = collider.varval[f'muxip6{bn}']
    muy_ir6 = collider.varval[f'muyip6{bn}']

    optimizers[f"ir6{bn}_run1"] = lm.rematch_ir6(
        collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=tw_sq_a45_ip5_a56[bn],
        boundary_conditions_right=arc_periodic_solution[f"lhc{bn}"]["67"],
        mux_ir6=mux_ir6_target[bn],
        muy_ir6=muy_ir6_target[bn],
        betx_ip6=betx_ip6,
        bety_ip6=bety_ip6,
        alfx_ip6=alfx_ip6,
        alfy_ip6=alfy_ip6,
        dx_ip6=dx_ip6,
        dpx_ip6=dpx_ip6,
        solve=False,
        staged_match=staged_match,
        default_tol=default_tol,
    )
    optimizers[f"ir6{bn}_run1"].targets[10].active = False
    optimizers[f"ir6{bn}_run1"].targets[11].active = False

    optimizers[f"ir6{bn}_run1"].solver.max_rel_penalty_increase = 2
    optimizers[f"ir6{bn}_run1"].solver.restore_if_fail = False
    optimizers[f"ir6{bn}_run1"].solve()

    optimizers[f"ir6{bn}_run2"] = lm.rematch_ir6(
        collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=tw_sq_a45_ip5_a56[bn],
        boundary_conditions_right=arc_periodic_solution[f"lhc{bn}"]["67"],
        mux_ir6=mux_ir6_target[bn],
        muy_ir6=muy_ir6_target[bn],
        betx_ip6=betx_ip6,
        bety_ip6=bety_ip6,
        alfx_ip6=alfx_ip6,
        alfy_ip6=alfy_ip6,
        dx_ip6=dx_ip6,
        dpx_ip6=dpx_ip6,
        solve=False,
        staged_match=staged_match,
        default_tol=default_tol,
    )
    optimizers[f"ir6{bn}_run2"].targets[10].active = True
    optimizers[f"ir6{bn}_run2"].targets[11].active = True

    optimizers[f"ir6{bn}_run2"].solver.max_rel_penalty_increase = 2
    optimizers[f"ir6{bn}_run2"].solver.restore_if_fail = False
    optimizers[f"ir6{bn}_run2"].solve()


udf.check_after_ir6_rematch(collider)

#%% Rematch IR8

udf.check_before_ir8_rematch(collider)

for bn in ['b1', 'b2']:
    alfx_ip8 = collider.varval[f'alfxip8{bn}']
    alfy_ip8 = collider.varval[f'alfyip8{bn}']
    betx_ip8 = collider.varval[f'betxip8{bn}']
    bety_ip8 = collider.varval[f'betyip8{bn}']
    dx_ip8 = collider.varval[f'dxip8{bn}']
    dpx_ip8 = collider.varval[f'dpxip8{bn}']
    mux_ir8 = collider.varval[f'muxip8{bn}']
    muy_ir8 = collider.varval[f'muyip8{bn}']

    optimizers[f"ir8{bn}"] = lm.rematch_ir8(
        collider,
        line_name=f"lhc{bn}",
        boundary_conditions_left=arc_periodic_solution[f"lhc{bn}"]["78"],
        boundary_conditions_right=tw_sq_a81_ip1_a12[bn],
        mux_ir8=mux_ir8_target[bn],
        muy_ir8=muy_ir8_target[bn],
        betx_ip8=betx_ip8,
        bety_ip8=bety_ip8,
        alfx_ip8=alfx_ip8,
        alfy_ip8=alfy_ip8,
        dx_ip8=dx_ip8,
        dpx_ip8=dpx_ip8,
        solve=True,
        staged_match=staged_match,
        default_tol=default_tol,
    )
udf.check_after_ir8_rematch(collider)
# %% Orbit knobs in ip2 ip8 
optimizers["orbit_knobs_ip28"] = lm.match_orbit_knobs_ip2_ip8(collider)
# %% Orbit knobs in ip1 ip5 
# NOTE: Not working with hl15, some naming differences
optimizers["orbit_knobs_ip15"] = lm_updated.match_orbit_knobs_ip1_ip5(collider)
#%%
udf.check_orbit_knobs(collider)

#%% Switch on sextupoles
print("Old sextupole values:\n")
udf.check_sext(collider)

ks = {"b1": {"f": 0.06, "d": -0.099}, "b2": {"f": 0.06, "d": -0.099}}
lm_updated.set_sext_all(collider, ks)

print("\nUpdated sextupoles:\n")
udf.check_sext(collider)

# %% Correct chromaticity with SF/SD sextupoles in sectors 23/34/56/67
optimizers['chromaweakb1_run1'], optimizers['chromaweakb2_run1'] = lm_updated.corchroma_weak(collider, 2, default_tol)
udf.check_chroma(collider)

# %% Rematch chromatic functions
optimizers["opt_w_b1"] =  lm_updated.rematch_w(collider, "lhcb1", default_tol, solve=True)
optimizers["opt_w_b2"] =  lm_updated.rematch_w(collider, "lhcb2", default_tol, solve=True)
tw = udf.check_W(collider)

# %% Correct chromaticity again with SF/SD sextupoles in sectors 23/34/56/67
optimizers['chromaweakb1_run2'], optimizers['chromaweakb2_run2'] = lm_updated.corchroma_weak(collider, 2, default_tol)
udf.check_chroma(collider)

# %%
print("on disp: ", collider.vars["on_disp"]._value)
udf.check_dispersion(collider)
optimizers['dispersion'] = lm_updated.rematch_disp(collider,solve=True,default_tol=default_tol,angle_ref=295,sep_ref=1,cycle=True)
udf.check_dispersion(collider)
print("on disp: ", collider.vars["on_disp"]._value)
# %%
optimizers['crabs'], _, _ = lm_updated.rematch_crabs(collider, z_crab=1e-3, nrj=7000, default_tol = { "x": 1e-18,"px": 1e-18,"y": 1e-18,"py": 1e-18}, reset_values=True, solve=True)
# %% 
lm_updated.mk_arc_trims(collider)


# %%
failed_status = []
for key in optimizers.keys():
    if ("arcs" in key) or ("orbit" in key) or ("dispersion" in key) or ("crabs" in key):
        for subkey in optimizers[key].keys():
            print(optimizers[key][subkey].log().rows[-1])
            mystatus = optimizers[key][subkey].log().tol_met[-1]
            if np.all(mystatus == "y"*len(mystatus)):
                print(f"{key} rematching was succesful!")
            else:
                failed_status.append(f"{key}_{subkey}")
                print(f"{key} rematching was NOT succesful!")
    else:
        print(optimizers[key].log().rows[-1])
        mystatus = optimizers[key].log().tol_met[-1]
        if np.all(mystatus == "y"*len(mystatus)):
            print(f"{key} rematching was succesful!")
        else:
            failed_status.append(key)
            print(f"{key} rematching was NOT succesful!")
print()
print("Failed rematch for: ")
for counter, i in enumerate(failed_status):
    print(counter, i)

# %%
